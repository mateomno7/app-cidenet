import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Empleado } from '../interfaces/empleado';

@Injectable({
  providedIn: 'root'
})
export class EmpleadosService {
  urlBase: string = 'http://localhost:8080/api/'
  constructor(private http: HttpClient) { }

  getEmpleados() {
    return this.http.get(this.urlBase + 'consultarempleados')
      .toPromise()
      .then((res: any) => <Empleado[]>res)
      .then((data: any) => { return data; });
  }

  saveEmpleado(empleado: Empleado) {
    return this.http.post(this.urlBase + 'guardarempleado', empleado, this.getOptions()).toPromise();
      // .then((res: any) => <Empleado[]>res)
      // .then((data: any) => { return data; });
  }

  deleteEmpleado(id: number) {
    return this.http.delete(this.urlBase + 'eliminarempleado/' + id, this.getOptions())
      .toPromise()
      .then((res: any) => <Empleado[]>res)
      .then((data: any) => { return data; });
  }

  private getOptions() {
    return { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) };
  }
}
