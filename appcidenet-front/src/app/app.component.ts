import { Component } from '@angular/core';
import { MenuItem } from 'primeng/api';
import { PrimeNGConfig } from 'primeng/api';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'appcidenet-front';

  items: MenuItem[] = [];

  constructor(private primengConfig: PrimeNGConfig) {}


  ngOnInit() {
    this.primengConfig.ripple = true;
    this.items = [
      { label: 'Home', icon: 'pi pi-fw pi-home' }
    ];
  }

}
