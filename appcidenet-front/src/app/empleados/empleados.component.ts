import { Component } from '@angular/core';
import { Empleado } from '../interfaces/empleado';
import { EmpleadosService } from '../services/empleados.service';
import { ConfirmationService, MessageService } from 'primeng/api';
import { Pais } from '../interfaces/pais';

@Component({
  selector: 'app-empleados',
  templateUrl: './empleados.component.html',
  styleUrls: ['./empleados.component.css'],
  providers: [ConfirmationService, MessageService]
})
export class EmpleadosComponent {

  formulario: any = {};
  empleados: Empleado[] = [];
  display: boolean = false;
  paises: Pais[];


  constructor(private empleadosService: EmpleadosService, private confirmationService: ConfirmationService, private messageService: MessageService) {
    this.paises = [
      { name: 'Colombia', code: 'COL' },
      { name: 'Estados Unidos', code: 'USA' }
    ];
  }

  ngOnInit() {
    this.empleadosService.getEmpleados().then((data: Empleado[]) => {
      this.empleados = data;
    });
  }

  showDialog() {
    this.display = true;
  }

  confirm(event: any, empleado: any) {
    this.confirmationService.confirm({
      target: event.target,
      message: "Está seguro de que desea eliminar el empleado?",
      icon: "pi pi-exclamation-triangle",
      accept: () => {
        this.empleadosService.deleteEmpleado(empleado.id).then((data: Empleado[]) => {
          location.reload();
        });
      },
      reject: () => {

      }
    });
  }

  validarFormulario(){  
    if(this.formulario['primerNombre'] === '' || this.formulario['primerNombre'] === undefined){
      this.messageService.add({severity:'error', summary: 'Error', detail: 'El Campo Primer Nombre es obligatorio'});
      return false;
    }else if(this.formulario['primerNombre'].length > 20){
      this.messageService.add({severity:'error', summary: 'Error', detail: 'El Campo Primer Nombre debe tener maximo 20 caracteres'});
      return false;
    }

    if(this.formulario['primerApellido'] === '' || this.formulario['primerApellido'] === undefined){
      this.messageService.add({severity:'error', summary: 'Error', detail: 'El Campo Primer Apellido es obligatorio'});
      return false;
    }else if(this.formulario['primerNombre'].length > 20){
      this.messageService.add({severity:'error', summary: 'Error', detail: 'El Campo Primer Nombre debe tener maximo 20 caracteres'});
      return false;
    }

    if(this.formulario['paisEmpleo'] === '' || this.formulario['paisEmpleo'] === undefined){
      this.messageService.add({severity:'error', summary: 'Error', detail: 'El Campo País del empleo es obligatorio'});
      return false;
    }

    if(this.formulario['otrosNombres'].length > 50){
      this.messageService.add({severity:'error', summary: 'Error', detail: 'El Campo Primer Nombre debe tener maximo 20 caracteres'});
      return false;
    }

    return true;
  }

  guardarEmpleado() {
    if(this.validarFormulario()){
      if(this.formulario['paisEmpleo'].code == 'USA'){
        this.formulario['correoElectronico'] = this.formulario['primerNombre'].toLowerCase() + '.' + this.formulario['primerApellido'].toLowerCase()  + '@cidenet.com.us';
      }else {
        this.formulario['correoElectronico'] = this.formulario['primerNombre'].toLowerCase() + '.' + this.formulario['primerApellido'].toLowerCase()  + '@cidenet.com.co';
      }

      this.validarCorreo(this.formulario['correoElectronico']);
      var p = this.formulario['paisEmpleo'].name
      this.formulario['paisEmpleo'] = null;
      this.formulario['paisEmpleo'] = p;
      this.formulario['primerNombre'] = this.formulario['primerNombre'].toUpperCase();
      this.formulario['primerApellido'] = this.formulario['primerApellido'].toUpperCase();
      this.formulario['otrosNombres'] = this.formulario['otrosNombres'].toUpperCase();

      this.empleadosService.saveEmpleado(this.formulario).then((response: any) => {
        location.reload();
      });
    }
  }

  validarCorreo(correo: string){

    this.empleados = this.empleados.filter(e => e.primerNombre.toUpperCase() == this.formulario['primerNombre'].toUpperCase() && e.primerApellido.toUpperCase() == this.formulario['primerApellido'].toUpperCase())
    if(this.empleados.length > 0){
      var i = this.empleados.length + 1;
      if(this.formulario['paisEmpleo'].code == 'USA'){
        this.formulario['correoElectronico'] = this.formulario['primerNombre'].toLowerCase() + '.' + this.formulario['primerApellido'].toLowerCase()  + '.' + i +'@cidenet.com.us';
      }else {
        this.formulario['correoElectronico'] = this.formulario['primerNombre'].toLowerCase() + '.' + this.formulario['primerApellido'].toLowerCase()  + '.' + i + '@cidenet.com.co';
      }
    }
  }

  mayuscula(e: any){
    e.target.value = e.target.value.toUpperCase();
  }
}
