export interface Empleado {
    id: number;
    primerNombre: string;
    otrosNombres: string;
    primerApellido: string;
    correoElectronico: string;
    paisEmpleo: string;
}
