package com.cidenet.appcidenetback.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.Table;

@Entity
@Table(name = "Empleado")
@SequenceGenerator(name="seq", initialValue=1, allocationSize=999999999)
public class Empleado {
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="seq")
    private int id;
	@Column(name = "primerNombre")
	private String primerNombre;
	@Column(name = "otrosNombres")
	private String otrosNombres;
	@Column(name = "primerApellido")
	private String primerApellido;
	@Column(name = "paisEmpleo")
	private String paisEmpleo;
	@Column(name = "correoElectronico")
	private String correoElectronico;

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getPrimerNombre() {
		return primerNombre;
	}
	public void setPrimerNombre(String primerNombre) {
		this.primerNombre = primerNombre;
	}
	public String getOtrosNombres() {
		return otrosNombres;
	}
	public void setOtrosNombres(String otrosNombres) {
		this.otrosNombres = otrosNombres;
	}
	public String getPrimerApellido() {
		return primerApellido;
	}
	public void setPrimerApellido(String primerApellido) {
		this.primerApellido = primerApellido;
	}
	public String getPaisEmpleo() {
		return paisEmpleo;
	}
	public void setPaisEmpleo(String paisEmpleo) {
		this.paisEmpleo = paisEmpleo;
	}
	public String getCorreoElectronico() {
		return correoElectronico;
	}
	public void setCorreoElectronico(String correoElectronico) {
		this.correoElectronico = correoElectronico;
	}

}
