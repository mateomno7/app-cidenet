package com.cidenet.appcidenetback.repositories;

import org.springframework.data.repository.CrudRepository;

import com.cidenet.appcidenetback.model.Empleado;

public interface IEmpleadoRepository extends CrudRepository<Empleado, Integer>{ }
