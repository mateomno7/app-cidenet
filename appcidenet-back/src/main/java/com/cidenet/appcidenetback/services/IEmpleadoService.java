package com.cidenet.appcidenetback.services;

import java.util.Set;

import org.springframework.stereotype.Service;

import com.cidenet.appcidenetback.model.Empleado;

@Service
public interface IEmpleadoService {
	public Empleado guardarEmpleado(Empleado empleado);
	public Set<Empleado> consultarEmpleados();
	public boolean eliminarEmpleado(int id);
}
