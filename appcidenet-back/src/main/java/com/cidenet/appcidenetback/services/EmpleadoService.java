package com.cidenet.appcidenetback.services;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cidenet.appcidenetback.model.Empleado;
import com.cidenet.appcidenetback.repositories.IEmpleadoRepository;

@Service
public class EmpleadoService implements IEmpleadoService {

	@Autowired
	IEmpleadoRepository empleadoRepository;

	@Override
	public Empleado guardarEmpleado(Empleado empleado) {
		try {
			return empleadoRepository.save(empleado);
		}catch(Exception ex) {
			return new Empleado();
		}
	}
	@Override
	public Set<Empleado> consultarEmpleados(){
		Set<Empleado> empleados = new HashSet<>();
		empleadoRepository.findAll().forEach(empleado -> empleados.add(empleado));
		return mapper(empleados);
	}

	@Override
	public boolean eliminarEmpleado(int id) {
		try {
			empleadoRepository.deleteById(id);
			return true;
		}catch(Exception ex) {
			return false;
		}
	}

	public Set<Empleado> mapper(Set<Empleado> empleados) {
		Set<Empleado> rs = new HashSet<>();
		for (Empleado c : empleados) {
			Empleado c2 = mapper(c);
			rs.add(c2);
		}
		return rs;
	}

	public Empleado mapper(Empleado c) {
		Empleado c2 = new Empleado();
		c2.setId(c.getId());
		c2.setPrimerNombre(c.getPrimerNombre());
		c2.setOtrosNombres(c.getOtrosNombres());
		c2.setPrimerApellido(c.getPrimerApellido());
		c2.setPaisEmpleo(c.getPaisEmpleo());
		c2.setCorreoElectronico(c.getCorreoElectronico());
		return c2;
	}
}
