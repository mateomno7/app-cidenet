package com.cidenet.appcidenetback.rest;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cidenet.appcidenetback.model.Empleado;
import com.cidenet.appcidenetback.services.IEmpleadoService;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api")
public class EmpleadoController {

	@Autowired
    IEmpleadoService empleadoService;

	@PostMapping("/guardarempleado")
	public ResponseEntity<Empleado> guardarEmpleado(@RequestBody Empleado empleado) {
		try {
			Empleado _empleado = empleadoService.guardarEmpleado(empleado);
			return new ResponseEntity<>(_empleado, HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

    @GetMapping("/consultarempleados")
    public Set<Empleado> consultarEmpleados() {
        return empleadoService.consultarEmpleados();
    }

    @DeleteMapping("/eliminarempleado/{id}")
    public ResponseEntity<HttpStatus>  elminarEmpleado(@PathVariable("id") int id) {
        try {
			empleadoService.eliminarEmpleado(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
    }
}
