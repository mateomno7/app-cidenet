package com.cidenet.appcidenetback;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AppcidenetBackApplication {

	public static void main(String[] args) {
		SpringApplication.run(AppcidenetBackApplication.class, args);
	}

}
